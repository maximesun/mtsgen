package com.anode.devone;

import javax.servlet.http.*;
import javax.servlet.*;
import java.io.*;

public class DemoServlet extends HttpServlet {
	private String message1;
	private String message2;

	public void init() throws ServletException {
		// Do required initialization
		message1 = "Hello,this is doGet.";
		message2 = "Hello,this is doPost.";
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Set response content type
		response.setContentType("text/html");

		// Actual logic goes here.
		PrintWriter out = response.getWriter();
		out.println("<h1>" + message1 + "</h1>");
	}

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Set response content type
		response.setContentType("text/html");

		// Actual logic goes here.
		PrintWriter out = response.getWriter();
		out.println("<h1>" + message2 + "</h1>");
	}

	public void destroy() {
		// do nothing.
	}

}
